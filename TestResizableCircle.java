package com.company.subtask3;

public class TestResizableCircle {
    public static void main(String[] args) {
        ResizableCircle MyTestResizableCircle = new ResizableCircle(5);
        System.out.println(MyTestResizableCircle.toString());
        System.out.println(MyTestResizableCircle.getArea());
        System.out.println(MyTestResizableCircle.getPerimeter());
        System.out.println("++++++++++++++++++++");
        Circle MyObject = new ResizableCircle(5);
        System.out.println(MyObject.getPerimeter());
        System.out.println(MyObject.getArea());
        System.out.println(MyObject.toString());
        Resizable MyNewObject = new ResizableCircle(5);
        MyNewObject.resize(4);
        System.out.println("++++++++++++++++++++");
        if (MyNewObject instanceof ResizableCircle){
            System.out.println(((ResizableCircle)MyNewObject).getArea());
        }
        if (MyNewObject instanceof Circle){
            System.out.println(((Circle)MyNewObject).getPerimeter());
        }
    }
}
