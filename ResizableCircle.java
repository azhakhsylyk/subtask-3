package com.company.subtask3;

public class ResizableCircle extends Circle implements Resizable {
    public ResizableCircle(double radius){
        super(radius);
    }

    public String toString() {
        return super.toString();
    }

    @Override
    public double resize(int percent) {
        return percent;
    }
}
