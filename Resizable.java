package com.company.subtask3;

public interface Resizable {
    public double resize(int percent);
}
